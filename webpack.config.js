module.exports = {
    entry: "./JSpider-webpack.js",
    output: {
        filename: "JSpider.js",
        path: __dirname + "/dist",
    },
    mode: "production",
};
